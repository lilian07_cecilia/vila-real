import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'categorias', loadChildren: './pg/categorias/categorias.module#CategoriasPageModule' },
  { path: 'inicio', loadChildren: './pg/inicio/inicio.module#InicioPageModule' },
  { path: 'avalie', loadChildren: './pg/avalie/avalie.module#AvaliePageModule' },
  { path: 'sobre', loadChildren: './pg/sobre/sobre.module#SobrePageModule' },
  { path: 'avalie', loadChildren: './pg/avalie/avalie.module#AvaliePageModule' },
  { path: 'lista-de-compras', loadChildren: './pg/lista-de-compras/lista-de-compras.module#ListaDeComprasPageModule' },
  { path: 'lista-mercearia', loadChildren: './pg/lista-mercearia/lista-mercearia.module#ListaMerceariaPageModule' },
  { path: 'lista-bebidas', loadChildren: './pg/lista-bebidas/lista-bebidas.module#ListaBebidasPageModule' },
  { path: 'lista-mistura', loadChildren: './pg/lista-mistura/lista-mistura.module#ListaMisturaPageModule' },
  { path: 'lista-frios', loadChildren: './pg/lista-frios/lista-frios.module#ListaFriosPageModule' },
  { path: 'lista-hortifruti', loadChildren: './pg/lista-hortifruti/lista-hortifruti.module#ListaHortifrutiPageModule' },
  { path: 'lista-padaria', loadChildren: './pg/lista-padaria/lista-padaria.module#ListaPadariaPageModule' },
  { path: 'lista-limpeza', loadChildren: './pg/lista-limpeza/lista-limpeza.module#ListaLimpezaPageModule' },
  { path: 'login', loadChildren: './pg/login/login.module#LoginPageModule' },
  { path: 'recuperar-senha', loadChildren: './pg/recuperar-senha/recuperar-senha.module#RecuperarSenhaPageModule' },
  { path: 'receita', loadChildren: './pg/receita/receita.module#ReceitaPageModule' },
  { path: 'servidor', loadChildren: './servidor/servidor.module#ServidorPageModule' },
  { path: 'url.service', loadChildren: './servidor/url.service/url.service.module#Url.ServicePageModule' },
  { path: 'url.service.spec.ts', loadChildren: './servidor/url.service.spec.ts/url.service.spec.ts.module#Url.Service.Spec.TsPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
