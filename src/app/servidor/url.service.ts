import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  url: string =
  'https://vilarealti04.000webhostapp.com/'

  constructor() { }

  pegarUrl() {
    return this.url;
  }
}
