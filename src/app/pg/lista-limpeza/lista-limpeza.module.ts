import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaLimpezaPage } from './lista-limpeza.page';

const routes: Routes = [
  {
    path: '',
    component: ListaLimpezaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaLimpezaPage]
})
export class ListaLimpezaPageModule {}
