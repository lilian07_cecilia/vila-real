import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaLimpezaPage } from './lista-limpeza.page';

describe('ListaLimpezaPage', () => {
  let component: ListaLimpezaPage;
  let fixture: ComponentFixture<ListaLimpezaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaLimpezaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaLimpezaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
