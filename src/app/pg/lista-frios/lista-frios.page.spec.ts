import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaFriosPage } from './lista-frios.page';

describe('ListaFriosPage', () => {
  let component: ListaFriosPage;
  let fixture: ComponentFixture<ListaFriosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaFriosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaFriosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
