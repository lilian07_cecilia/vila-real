import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaHortifrutiPage } from './lista-hortifruti.page';

const routes: Routes = [
  {
    path: '',
    component: ListaHortifrutiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaHortifrutiPage]
})
export class ListaHortifrutiPageModule {}
