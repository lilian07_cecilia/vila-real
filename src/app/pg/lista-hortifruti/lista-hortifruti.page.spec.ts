import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaHortifrutiPage } from './lista-hortifruti.page';

describe('ListaHortifrutiPage', () => {
  let component: ListaHortifrutiPage;
  let fixture: ComponentFixture<ListaHortifrutiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaHortifrutiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaHortifrutiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
