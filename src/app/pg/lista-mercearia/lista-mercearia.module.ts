import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaMerceariaPage } from './lista-mercearia.page';

const routes: Routes = [
  {
    path: '',
    component: ListaMerceariaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaMerceariaPage]
})
export class ListaMerceariaPageModule {}
