import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMerceariaPage } from './lista-mercearia.page';

describe('ListaMerceariaPage', () => {
  let component: ListaMerceariaPage;
  let fixture: ComponentFixture<ListaMerceariaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMerceariaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMerceariaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
