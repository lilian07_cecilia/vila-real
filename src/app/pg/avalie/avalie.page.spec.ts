import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvaliePage } from './avalie.page';

describe('AvaliePage', () => {
  let component: AvaliePage;
  let fixture: ComponentFixture<AvaliePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvaliePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvaliePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
